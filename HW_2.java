package day2;

public class HW_2 {

		public static void main(String[] args) {
			TN();
			System.out.println("<----------------^-------------->");
			TN2();
		}

		public static void TN() {
			String[][] table = { { "1", "2", "3" }, { "4", "5", "6" }, { "7", "8", "9" } };
			int[] nTable = new int[table.length];
			for (int row = 0; row < table.length; row++) {
				for (int element1 = 0; element1 < table.length; element1++) {
					nTable[row] = Integer.parseInt(table[row][element1]);
					System.out.printf("%3d", nTable[row]);
				}
				System.out.println("");
			}
		}

		public static void TN2() {
			String[][] table = { { "1", "2", "3" }, { "4", "5", "6" }, { "7", "8", "9" } };
			int[] nTable = new int[table.length];
			for (int row = 0; row < table.length; row++) {
				for (int element1 = 0; element1 < table.length; element1++) {
					nTable[row] = Integer.parseInt(table[row][element1]);
					System.out.printf("%3d", nTable[row] * 2);
				}
				System.out.println("");
			}
		}
	
}
