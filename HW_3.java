package day2;

public class HW_3 {

	public static void main(String[] args) {
		System.out.println("  No.3.1 --------------------------------");
		drawprint9(4);
		Space_1();
		System.out.println("  No.3.2 --------------------------------");
		drawprint10(4);
		Space_1();
		System.out.println("  No.3.3 --------------------------------");
		drawprint11(4);
		Space_1();
		System.out.println("  No.3.4 --------------------------------");
		drawprint12(4);
		Space_1();
		System.out.println("  No.3.5 --------------------------------");
		drawprint13(4);
		Space_1();
		System.out.println("  No.3.6 --------------------------------");
		drawprint14(4);
		Space_1();
		System.out.println("  No.3.7 --------------------------------");
		drawprint15(4);
		Space_1();
		System.out.println("  No.3.8 --------------------------------");
		drawprint16(4);
		Space_1();
		System.out.println("  No.3.9 --------------------------------");
		drawprint17(4);
		Space_1();
	}

	public static void drawprint9(int n) {
		for (int i = 1; i <= 1; i++) {
			for (int j = 0; j < n; j++) {
				System.out.println(" " + 2 * j);
			}
		}
		System.out.println(" n: " + n);
	}

	public static void drawprint10(int n) {
		for (int i = 1; i <= 1; i++) {
			for (int j = 1; j <= n; j++) {
				System.out.println(" " + 2 * j);
			}
		}
		System.out.println(" n: " + n);
	}

	public static void drawprint11(int n) {
		for (int i = 1; i <= n; i++) {
			for (int j = 1; j <= n; j++) {
				System.out.print("  " + i * j);
			}
			System.out.printf("\n");
		}
		System.out.println(" n: " + n);
	}

	public static void drawprint12(int n) {
		String[] result = new String[n];
		int num;
		for (num = 0; num < n; num++)
			result[num] = " *";
		for (num = 0; num < n; num++) {
			result[num] = " -";
			for (int num2 = 0; num2 < n; num2++)
				System.out.print(result[num2]);
			result[num] = " *";

			System.out.println();
		}
	}

	public static void drawprint13(int n) {
		String[] s = new String[n];
		int i;
		for (i = 0; i < n; i++)
			s[i] = " *";
		for (i = n; i > 0; i--) {
			s[i - 1] = " -";
			for (int j = 0; j < n; j++)
				System.out.print(s[j]);
			s[i - 1] = " *";
			System.out.println();
		}
	}

	public static void drawprint14(int n) {
		for (int i = 0; i < n; i++) {
			for (int j = 0; j <= i; j++) {
				System.out.print(" *");
			}
			for (int j = i; j < n - 1; j++) {
				System.out.print(" -");
			}
			System.out.println(" ");
		}
		System.out.println(" n: " + n);
	}

	public static void drawprint15(int n) {
		for (int i = 1; i <= n; i++) {
			for (int j = i; j <= n; j++) {
				System.out.print(" *");
			}
			for (int j = 1; j <= i - 1; j++) {
				System.out.print(" -");
			}
			System.out.println(" ");
		}
		System.out.println(" n: " + n);
	}

	public static void drawprint16(int n) {
		for (int i = 0; i < n; i++) {
			for (int j = 0; j <= i; j++) {
				System.out.print(" *");
			}
			for (int j = i; j <= n - 2; j++) {
				System.out.print(" -");
			}
			System.out.println(" ");
		}
		for (int i = 1; i <= n - 1; i++) {
			for (int j = 1; j <= (n - i); j++) {
				System.out.print(" *");
			}
			for (int j = 1; j <= i; j++) {
				System.out.print(" -");
			}
			System.out.println(" ");
		}
		System.out.println(" n: " + n);
	}

	public static void drawprint17(int n) {
		for (int i = 0; i < n; i++) {
			for (int j = 0; j <= i; j++) {
				System.out.print(" " + (i + 1));
			}
			for (int j = i; j <= n - 2; j++) {
				System.out.print(" -");
			}
			System.out.println(" ");
		}
		for (int i = 1; i <= n - 1; i++) {
			for (int j = 1; j <= (n - i); j++) {
				System.out.print(" " + (4 - i));
			}
			for (int j = 1; j <= i; j++) {
				System.out.print(" -");
			}
			System.out.println(" ");
		}
		System.out.println(" n: " + n);
	}

	public static void Space_1() {
		System.out.println(" ");
	}
	
}
