package day2;

public class HW_8 {
	
		public static void main(String[] args) {
			System.out.println("  No.1.1 --------------------------------");
			drawprint1(4);
			space();
			System.out.println("  No.1.2 --------------------------------");
			drawprint2(4);
			space();
			System.out.println("  No.1.3 --------------------------------");
			drawprint3(4);
			space();
			System.out.println("  No.1.4 --------------------------------");
			drawprint4(4);
			space();
			System.out.println("  No.1.5 --------------------------------");
			drawprint5(4);
			space();
			System.out.println("  No.1.6 --------------------------------");
			drawprint6(4);
			space();
			System.out.println("  No.1.7 --------------------------------");
			drawprint7(4);
			space();
			System.out.println("  No.1.8 --------------------------------");
			drawprint8(4);
			space();
		}

		public static void drawprint1(int n) {
			for (int j = 0; j < n; j++) { // ใช้ควบคุมจำนวน * เท่ากับ count
				System.out.print(" *"); // ใช้วาดรูป *
			}
			System.out.println();
			System.out.println(" n: " + n); // เมื่อจบแต่ละบรรทัดขึ้นบรรทัดใหม
		}

		public static void drawprint2(int n) {
			for (int i = 0; i < n; i++) { // ใช้ควบคุมจำนวนบรรทัดเท่ากับ count
				for (int j = 0; j < n; j++) { // ควบคุมจำนวน * เท่ากับ count
					System.out.print(" *"); // ใช้วาดรูป *
				}
				System.out.println(); // เมื่อจบแต่ละบรรทัดขึ้นบรรทัดใหม่
			}
			System.out.println(" n: " + n); // เมื่อวาดเสร็จทำการขึ้นบรรทัดใหม่
		}

		public static void drawprint3(int n) {
			for (int i = 1; i <= n; i++) {
				for (int j = 1; j <= n; j++) {
					System.out.print(j);
				}
				System.out.println();
			}
			System.out.println(" n: " + n);
		}

		public static void drawprint4(int n) {
			for (int i = 1; i <= n; i++) {
				for (int j = n; j >= 1; j--) {
					System.out.print(j);
				}
				System.out.println();
			}
			System.out.println(" n: " + n);
		}

		public static void drawprint5(int n) {
			for (int i = 1; i <= n; i++) {
				for (int j = 0; j < n; j++) {
					System.out.print(i);
				}
				System.out.println();
			}
			System.out.println(" n: " + n);
		}

		public static void drawprint6(int n) {
			for (int i = n; i >= 1; i--) {
				for (int j = 1; j <= n; j++) {
					System.out.print(i);
				}
				System.out.println();
			}
			System.out.println(" n: " + n);
		}

		public static void drawprint7(int n) {
			int k = 1;
			for (int i = 1; i <= n; i++) {
				for (int j = 1; j <= n; j++, k++) {
					System.out.printf(" %-3d", k);
				}
				System.out.print("\n");
			}
			System.out.println(" n: " + n);
		}

		public static void drawprint8(int n) {
			int k = 16;
			for (int i = 1; i <= n; i++) {
				for (int j = 1; j <= n; j++, k--) {
					System.out.printf(" %-3d", k);
				}
				System.out.print("\n");
			}
			System.out.println(" n: " + n);
		}

		public static void space() {
			System.out.println(" ");
		}
	}